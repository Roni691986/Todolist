import Vue from 'vue'
import Router from 'vue-router'
import VueCookie from 'vue-cookie'

Vue.use(VueCookie);
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
    },
  ]
})
